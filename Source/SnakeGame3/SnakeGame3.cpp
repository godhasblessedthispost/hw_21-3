// Copyright Epic Games, Inc. All Rights Reserved.

#include "SnakeGame3.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, SnakeGame3, "SnakeGame3" );
