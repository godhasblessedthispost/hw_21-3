// Fill out your copyright notice in the Description page of Project Settings.


#include "FoodSpawner.h"
#include "Food.h"

// Sets default values
AFoodSpawner::AFoodSpawner()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	Foodptr = NULL;
}

// Called when the game starts or when spawned
void AFoodSpawner::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AFoodSpawner::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	CreateFoodActor();
}

void AFoodSpawner::CreateFoodActor()
{
	if (Foodptr == NULL)
	{
		FVector NewLocation(Rando() * 60, Rando() * 60, 0);
		FTransform NewTransform(NewLocation);
		Foodptr = GetWorld()->SpawnActor<AFood>(FoodActorClass, NewTransform);
	}
}

void AFoodSpawner::Fdel()
{
	Foodptr = NULL;
}

float AFoodSpawner::Rando()
{
	float numero;
	numero = -5 + rand() % (5 - (-5) + 1);
		return numero;
}
