// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SnakeGame3GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SNAKEGAME3_API ASnakeGame3GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
